package pnj.latihan.Muhammad_Ridwan_Ramadhan.smsreceiver;

public interface MessageListener {

    void messageReceived(String message);
}
